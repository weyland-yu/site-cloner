#!/usr/bin/env bash

# Pass necessary arguments to variables
NAME=$1
SITE=$2

# Copy the specified URL
wget --mirror --convert-links $SITE

# Make a variable out of the name of the newly created path, echo it
SITE_PATH=$(ls -St | head -n1)
echo "The site path is" $SITE_PATH

# Append newly created directory to local repo HTML 
./html_adder.py "$NAME" $SITE_PATH
