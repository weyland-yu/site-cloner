#!/usr/bin/env python3

import sys
from bs4 import BeautifulSoup

# Open the existing target HTML file, assign its contents to a variable
file = open("local_repo.html", "r")
contents = file.read()
file.close()

# Declare CLI argument variables
commonName = str(sys.argv[1])
mirroredUrl = str(sys.argv[2])

# Declare the "soup" variable
soup = BeautifulSoup(contents, "lxml")

# Declare "table" as the table containing relevant links
table = soup.table

# Open the new row
newRow = soup.new_tag("tr")

# Start the first cell
titleCell = soup.new_tag("td")
titleCell.append(commonName)
newRow.insert(1, titleCell)

# Start the second cell
linkCell = soup.new_tag("td")

# Create the link for the second cell
aHref = soup.new_tag("a")
aHref["href"] = "./" + mirroredUrl + "/index.html"
aHref.append(mirroredUrl)

# Finish the second cell
linkCell.insert(1, aHref)
newRow.insert(1, linkCell)

# Append the row to the table
table.append(newRow)

# Save the changes to the local HTML file
savechanges = soup.prettify("utf-8")
with open("local_repo.html", "wb") as file:
    file.write(savechanges)
